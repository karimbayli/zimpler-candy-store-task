package entity

//CandyDataSource - it is dto of task data
type CandyDataSource struct {
	Name  string
	Candy string
	Eaten int
}

//========
type SnackInfo struct {
	TotalSum int
	Counts   []int
}
type ClientCandyOrders struct {
	Snack      map[string]SnackInfo
	TotalSnack int
}
type Clients struct {
	Name           string `json:"name"`
	FavouriteSnack string `json:"favouriteSnack"`
	TotalSnacks    int    `json:"totalSnacks"`
}
type Result []Clients

//========
