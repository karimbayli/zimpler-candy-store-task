package v1

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"zimpler-candy-store-task/internal/usecase"
	"zimpler-candy-store-task/pkg/logger"
)

type calculationRoutes struct {
	c usecase.CandyStore
	l logger.Interface
}

func newCalculationRoutes(handler *gin.RouterGroup, c usecase.CandyStore, l logger.Interface) {
	r := &calculationRoutes{c, l}

	h := handler.Group("/candyStore")
	{
		h.GET("/topCustomers", r.topCustomers)
		//h.POST("/test", r.doTranslate)
	}

}

// @Summary     Show result
// @Description Show calculated candyStore data
// @ID          topCustomers
// @Tags  	    topCustomers
// @Accept      json
// @Produce     json
// @Success     200 {object} entity.Result
// @Failure     500 {object} response
// @Router      /candyStore/topCustomers [get]
func (r *calculationRoutes) topCustomers(ctx *gin.Context) {
	result, err := r.c.FindTopCustomerAndFavouriteSnack(ctx.Request.Context())
	if err != nil {
		r.l.Error(err, "http - v1 - topCustomers- ")
		errorResponse(ctx, http.StatusInternalServerError, "v1 topCustomers")
		return
	}

	ctx.JSON(http.StatusOK, result)
}
