package v1

import (
	"zimpler-candy-store-task/internal/usecase"
	"zimpler-candy-store-task/pkg/logger"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "zimpler-candy-store-task/docs"
	// Swagger docs.
)

// NewRouter -.
// Swagger spec:
// @title       Candy Store API
// @description Candy Store Test Task
// @version     1.0
// @host        localhost:8080
// @BasePath    /v1
func NewRouter(handler *gin.Engine, c usecase.CandyStore, l logger.Interface) {
	// Options
	handler.Use(gin.Logger())
	handler.Use(gin.Recovery())

	// Swagger
	swaggerHandler := ginSwagger.DisablingWrapHandler(swaggerFiles.Handler, "DISABLE_SWAGGER_HTTP_HANDLER")
	handler.GET("/swagger/*any", swaggerHandler)

	// Routers
	h := handler.Group("/v1")
	{
		newCalculationRoutes(h, c, l)
	}
}
