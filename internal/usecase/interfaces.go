package usecase

import (
	"context"
	"zimpler-candy-store-task/internal/entity"
)

type (
	CandyStore interface {
		FindTopCustomerAndFavouriteSnack(ctx context.Context) (entity.Result, error)
	}
	//Parser -
	Parser interface {
		ParseHtml(ctx context.Context) ([]string, error)
	}

	Calculator interface {
		CalculateCandyData(bulkCandyData []string) (entity.Result, error)
	}
)
