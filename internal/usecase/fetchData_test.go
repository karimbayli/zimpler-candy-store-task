package usecase

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"testing"
	"zimpler-candy-store-task/internal/entity"
)

var errEmptyDataErr = errors.New("empty data")

type test struct {
	name string
	mock func()
	res  interface{}
	err  error
}

func fetchData(t *testing.T) (*FetchDataUseCase, *MockCalculator, *MockParser) {
	t.Helper()
	mockCtl := gomock.NewController(t)
	defer mockCtl.Finish()

	parser := NewMockParser(mockCtl)
	calculator := NewMockCalculator(mockCtl)
	fetchData := New(parser, calculator)

	return fetchData, calculator, parser
}
func TestFindTopCustomerAndFavouriteSnack(t *testing.T) {
	fetchData, calc, parser := fetchData(t)

	tests := []test{
		{
			name: "empty result",
			mock: func() {
				parser.EXPECT().ParseHtml(context.Background()).Return(nil, nil)
				calc.EXPECT().CalculateCandyData(nil).Return(nil, errEmptyDataErr)
			},
			res: entity.Result(nil),
			err: errEmptyDataErr,
		},
	}

	for _, tc := range tests {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			tc.mock()

			res, err := fetchData.FindTopCustomerAndFavouriteSnack(context.Background())

			require.Equal(t, res, tc.res)
			require.ErrorIs(t, err, tc.err)
		})
	}
}
