package usecase

import (
	"context"
	"fmt"
	"zimpler-candy-store-task/internal/entity"
)

type FetchDataUseCase struct {
	parser     Parser
	calculator Calculator
}

func (fu *FetchDataUseCase) FindTopCustomerAndFavouriteSnack(ctx context.Context) (entity.Result, error) {
	candyDataList, err := fu.parser.ParseHtml(ctx)
	if err != nil {
		return nil, fmt.Errorf("CalculateUseCase - Parse - html-parser.ParseHtml: %w", err)
	}
	result, err := fu.calculator.CalculateCandyData(candyDataList)
	if err != nil {
		return nil, fmt.Errorf("CalculateUseCase - Parse - calculator.CalculateCandyData: %w", err)
	}
	return result, nil
}

func New(p Parser, calculator Calculator) *FetchDataUseCase {
	return &FetchDataUseCase{
		parser:     p,
		calculator: calculator,
	}
}

/*func (fu *FetchDataUseCase) Calculate(ctx context.Context) (entity.Result, error) {
	candyDataList, err := fu.ParseHtml(ctx)
	if err != nil {
		return nil, fmt.Errorf("CalculateUseCase - Parse - html-parser.ParseHtml: %w", err)
	}
	result, err := fu.calculator.CalculateCandyData(candyDataList)
	if err != nil {
		return nil, fmt.Errorf("CalculateUseCase - Parse - calculator.CalculateCandyData: %w", err)
	}
	return result, nil

}

func (fu *FetchDataUseCase) ParseHtml(ctx context.Context) ([]string, error) {
	candyDataList, err := fu.parser.ParseHtml(ctx)
	if err != nil {
		return nil, fmt.Errorf("CalculateUseCase - Parse - html-parser.ParseHtml: %w", err)
	}
	return candyDataList, nil

}
*/
