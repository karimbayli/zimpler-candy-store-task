package html_parser

import (
	"bytes"
	"context"
	"golang.org/x/net/html"
	"io"
	"net/http"
	"strings"
)

type ParserConf struct {
	Url string
}

type Parser struct {
	conf ParserConf
}

func New(url string) *Parser {
	return &Parser{conf: ParserConf{Url: url}}
}

func (p *Parser) ParseHtml(ctx context.Context) ([]string, error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, "GET", p.conf.Url, nil)
	if err != nil {
		return nil, err
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	reader := strings.NewReader(string(b))
	doc, err := html.Parse(reader)
	if err != nil {
		return nil, err
	}

	r1 := getElementById(doc, "top.customers")
	//rl := getElementById(r1, "tbody")
	//fmt.Println(r1)
	var by bytes.Buffer
	html.Render(&by, r1)
	//fmt.Println(by.String())

	z := html.NewTokenizer(strings.NewReader(by.String()))
	var content []string

	// While have not hit the </html> tag
	for z.Token().Data != "table" {
		tt := z.Next()
		if tt == html.StartTagToken {
			t := z.Token()
			if t.Data == "td" {
				inner := z.Next()
				if inner == html.TextToken {
					text := (string)(z.Text())
					t := strings.TrimSpace(text)
					content = append(content, t)
				}
			}
		}
	}
	return content, nil
}

func GetAttribute(n *html.Node, key string) (string, bool) {
	for _, attr := range n.Attr {
		if attr.Key == key {
			return attr.Val, true
		}
	}
	return "", false
}

func checkId(n *html.Node, id string) bool {
	if n.Type == html.ElementNode {
		s, ok := GetAttribute(n, "id")
		if ok && s == id {
			return true
		}
	}
	return false
}

func traverse(n *html.Node, id string) *html.Node {
	if checkId(n, id) {
		return n
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		result := traverse(c, id)
		if result != nil {
			return result
		}
	}

	return nil
}

func getElementById(n *html.Node, id string) *html.Node {
	return traverse(n, id)
}
