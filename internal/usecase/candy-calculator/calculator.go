package candy_calculator

import (
	"errors"
	"strconv"
	"zimpler-candy-store-task/internal/entity"
)

type Calculator struct {
}

func New() *Calculator {
	return &Calculator{}
}

func (c *Calculator) CalculateCandyData(bulkCandyData []string) (entity.Result, error) {
	if len(bulkCandyData) == 0 || bulkCandyData == nil {
		return nil, errors.New("empty data")
	}
	processedCandyData, err := c.prepareDataForCalculation(bulkCandyData)
	if err != nil {
		return nil, err
	}
	processedCandyDataInMap := c.calculateCandyData(processedCandyData)
	result := c.FindFavoriteSnackForCalculatedCanData(processedCandyDataInMap)
	return result, err

}

func (c *Calculator) FindFavoriteSnackForCalculatedCanData(calculatedDataInMap map[string]*entity.ClientCandyOrders) entity.Result {
	res := entity.Result{}
	for k, v := range calculatedDataInMap {
		client := entity.Clients{
			Name:           k,
			FavouriteSnack: rankByEatenCount(calculatedDataInMap[k]),
			TotalSnacks:    v.TotalSnack,
		}
		res = append(res, client)
	}
	return res
}

//PrepareDataForCalculation - this function divides bulk data into block(size=3 {name, snackName, eaten})
func (c *Calculator) prepareDataForCalculation(bulkCandyData []string) ([]entity.CandyDataSource, error) {
	var candies []entity.CandyDataSource
	var candy = entity.CandyDataSource{}
	var name string
	var candyName string
	var eaten int

	for i, v := range bulkCandyData {
		if i == 0 || (i+1)%3 == 1 {
			name = v
			continue
		}
		if (i+1)%3 == 2 {
			candyName = v
			continue
		}
		if (i+1)%3 == 0 {
			n, err := strconv.Atoi(v)
			if err != nil {
				return nil, err
			}
			eaten = n
			candy = entity.CandyDataSource{
				Name:  name,
				Candy: candyName,
				Eaten: eaten,
			}
			candies = append(candies, candy)
		}
	}
	return candies, nil
}

func (c *Calculator) calculateCandyData(processedCandyData []entity.CandyDataSource) map[string]*entity.ClientCandyOrders {
	generalCandyMap := make(map[string]*entity.ClientCandyOrders)

	for _, v := range processedCandyData {
		inner, ok := generalCandyMap[v.Name]
		if !ok {
			inner = new(entity.ClientCandyOrders)
			inner.Snack = make(map[string]entity.SnackInfo)
			generalCandyMap[v.Name] = inner
			generalCandyMap[v.Name].Snack[v.Candy] = inner.Snack[v.Candy]
		}

		candyInfo, ok := generalCandyMap[v.Name].Snack[v.Candy]
		if !ok {
			var newArray []int
			newArray = append(newArray, v.Eaten)
			generalCandyMap[v.Name].Snack[v.Candy] = entity.SnackInfo{
				TotalSum: v.Eaten,
				Counts:   newArray,
			}

		}
		generalCandyMap[v.Name].Snack[v.Candy] = entity.SnackInfo{
			TotalSum: candyInfo.TotalSum + v.Eaten,
			Counts:   append(candyInfo.Counts, v.Eaten),
		}

		generalCandyMap[v.Name].TotalSnack += v.Eaten
	}
	return generalCandyMap
}
