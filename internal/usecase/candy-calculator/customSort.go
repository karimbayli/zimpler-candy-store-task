package candy_calculator

import (
	"sort"
	"zimpler-candy-store-task/internal/entity"
)

func rankByEatenCount(wordFrequencies *entity.ClientCandyOrders) string {
	//a := len(wordFrequencies.candy)
	pl := make(PairList, len(wordFrequencies.Snack))
	i := 0
	for k, v := range wordFrequencies.Snack {
		pl[i] = Pair{k, v.TotalSum}
		i++
	}
	sort.Sort(sort.Reverse(pl))
	return pl[0].Key
}

type Pair struct {
	Key   string
	Value int
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
