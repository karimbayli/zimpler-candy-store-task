package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	"os/signal"
	"syscall"
	"zimpler-candy-store-task/config"
	v1 "zimpler-candy-store-task/internal/controller/http/v1"
	"zimpler-candy-store-task/internal/usecase"
	candy_calculator "zimpler-candy-store-task/internal/usecase/candy-calculator"
	html_parser "zimpler-candy-store-task/internal/usecase/html-parser"
	"zimpler-candy-store-task/pkg/httpserver"
	"zimpler-candy-store-task/pkg/logger"
)

func Run(cfg *config.Config) {
	l := logger.New(cfg.Log.Level)

	// Use case
	calculationUseCase := usecase.New(html_parser.New(cfg.Url.CandyStore), candy_calculator.New())

	// HTTP Server
	handler := gin.New()
	v1.NewRouter(handler, calculationUseCase, l)
	httpServer := httpserver.New(handler, httpserver.Port(cfg.HTTP.Port))

	// Waiting signal
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-interrupt:
		l.Info("app - Run - signal: " + s.String())
	case err := <-httpServer.Notify():
		l.Error(fmt.Errorf("app - Run - httpServer.Notify: %w", err))
	}

	// Shutdown
	err := httpServer.Shutdown()
	if err != nil {
		l.Error(fmt.Errorf("app - Run - httpServer.Shutdown: %w", err))
	}

}
