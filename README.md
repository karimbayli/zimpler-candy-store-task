# 
## Zimpler test assignment

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Requirements
https://candystore.zimpler.net/
Your goal is to extract the top customers, and their favourite snacks.





## How to run program
Run example:
```sh
make run
```
`make help` display commands

## Swagger url:
http://localhost:8080/swagger/index.html#/

## Curl:
```sh
curl -X 'GET' \
  'http://localhost:8080/v1/candyStore/topCustomers' \
  -H 'accept: application/json'
```

## Configuration file:
location: config/config.yml
in ```config.yml``` -  http server port: ``8080``

## Thanks:
Dear developer, **thank you for your time.** I hope my code didn't scare you :)

## License

MIT

**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[dill]: <https://github.com/joemccann/dillinger>
[git-repo-url]: <https://github.com/joemccann/dillinger.git>


